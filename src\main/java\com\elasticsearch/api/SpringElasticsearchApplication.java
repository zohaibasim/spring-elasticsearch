package com.elasticsearch.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.elasticsearch.model.Customer;
import com.elasticsearch.repository.CustomerRepository;

@SpringBootApplication
@RestController
public class SpringElasticsearchApplication {
	
	@Autowired
	private CustomerRepository repo;
	
	@PostMapping("/saveCustomer")
	public int saveCustomer(@RequestBody List<Customer> customers) {
		repo.saveAll(customers);
		return customers.size();
	}
	
	@GetMapping("/findAll")
	public Iterable<Customer> findAllCustomers(){
		return repo.findAll();
	}
	
	@GetMapping("/findByFName/{firstName}")
	public List<Customer> findByFirstName(@PathVariable String firstName){
		return repo.findByFirstName(firstName);
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringElasticsearchApplication.class, args);
	}

}
